/*
Escribe una consulta que inserte una columna (new_col INT) 
en una tabla hive(table_name) después de una existente (x_col)
*/
--- First add the new column
ALTER TABLE table_name ADD COLUMNS (new_col INT);
--- next step change column order
ALTER TABLE table_name CHANGE COLUMN new_col new_col INT AFTER x_col);
