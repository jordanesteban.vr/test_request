# ----- Preguntas HIVE -----
## Pregunta 7
Escribe una consulta que inserte en la tabla hive "empleados" una 
partición cuyos valores sean cargados desde tabla "staged_employees". Lo que importa es la estructura de la consulta no los valores de cada columna.

***

## Pregunta 8
Escribe una consulta que inserte una columna (new_col INT) en una tabla hive(table_name) después de una existente (x_col)
