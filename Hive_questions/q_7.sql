/*
    Escribe una consulta que inserte en la tabla hive "empleados" 
    una partición cuyos valores sean cargados desde tabla 
    "staged_employees". Lo que importa es la estructura de la consulta no los valores de cada columna.
 */
INSERT INTO TABLE empleados PARTITION (PARTITION_name)
SELECT name
FROM staged_employees;