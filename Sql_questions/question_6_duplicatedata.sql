/*
 * Eliminar Duplicados en La base de datos, BASADO en las columnas nombre,apellido,genero,Salario,fechaIngreso 
 */
DELETE FROM
    public.employees emp_a
        USING public.employees emp_b
WHERE
    a.ID < b.ID
    AND emp_a.nombre = emp_b.nombre
    and emp_a.apellido = emp_b.apellido
    and emp_a.genero = emp_b.genero 
    and emp_a.Salario = emp_b.Salario
    and emp_a.fechaIngreso = emp_b.fechaIngreso;