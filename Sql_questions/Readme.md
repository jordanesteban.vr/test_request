# ---- Preguntas SQL ----
##	Pregunta 5
Dada la siguiente matriz, escribe una consulta para encontrar los empleados que se contrataron en los últimos N=100 días. 
Sobre la misma tabla escribe una consulta para encontrar los empleados que se contrataron en los últimos N=2 años.
```
ID,nombre,apellido,genero,Salario,fechaIngreso
1,Rajveer,Singh,Male,30000,05-11-2018
2,Manveer,Singh,Male,50000,05-09-2017
3,Ashutosh,Kumar,Male,40000,12-12-2019
4,Ankita,Sharma,Female,45000,15-12-2019
5,Vijay,Kumar,Male,50000,12-01-2020
6,Dilip,Yadav,Male,25000,26-02-2020
7,Jayvijay,Singh,Male,30000,18-02-2020
8,Reenu,Kumari,Female,40000,19-09-2019
9,Ankit,Verma,Male,25000,04-04-2018
10,Harpreet,Singh,Male,50000,10-10-2020
```
### Respuesta: question_5.sql
***

##	Pregunta 6
Dada la siguiente matriz, construir una consulta que elimine los datos duplicados.
```
ID,NAME,SALARY
1,Harpreet,20000
2,Ravi,30000
3,Vinay,10000
4,Ravi,30000
5,Harpreet,20000
6,Vinay,10000
7,Rajeev,40000
8,Vinay,10000
9,Ravi,30000
10,Sanjay,50000
```
### Respuesta: question_6_duplicatedata.sql