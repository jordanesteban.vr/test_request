/*
 * Encontrar los empleados que se contrataron en los últimos N=100 días
 */
SELECT emp.ID, emp.fechaIngreso 
FROM public.employees emp 
WHERE to_char(emp.fechaIngreso, 'YYYY/MM/DD') > current_date - interval '100' day;