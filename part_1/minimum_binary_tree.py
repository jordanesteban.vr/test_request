class Node: 
  def __init__(self , val): 
      self.value = val  
      self.left = None
      self.right = None

def minimumDepth(curr):
  #Null node has 0 depth.
  if curr == None:
    return 0

  # Leaf node reached.
  if curr.left == None and curr.right == None:
    return 1

  # Current node has only right subtree.
  if not curr.left:
    return 1 + minimumDepth(curr.right)
  
  # Current node has only left subtree.
  elif not curr.right:
    return 1 + minimumDepth(curr.left)
  
  # if none of the above cases, then recur on both left and right subtrees.
  return 1 + min(minimumDepth(curr.left), minimumDepth(curr.right))

# Driver code
root = Node(1)
root.left = Node(3)
root.right = Node(2)
root.left.left = Node(4)

print("The min depth is between Node {} AND {}:".format(root.value, minimumDepth(root))) 