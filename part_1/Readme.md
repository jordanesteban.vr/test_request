#	Pregunta 1

Dada una matriz 
```
1  2  3  4
5  6  7  8
9  10 11 12
13 14 15 16
```
Construir una función que recorra la matriz en forma espiral.

### Respuesta: spiral_matrix.py

***

#	Pregunta 2

Dada dos secuencias de caracteres, crear una función que encuentre las subsecuencias más largas presentes en ambas.
Ejemplo:
```
Input:
A = 6, B = 6
str1 = ABCDGH
str2 = AEDGHR
Output: 3 -> DGH
```

### Respuesta: LCS_problem.py
***

#	Pregunta 3

Construir una función que dado un árbol binario, determine profundidad mínima.
Ejemplo 1: la profundidad mínima es entre nodo 1 y nodo 2
```
            1
          /   \
         3     2
        /
       4          
```
### Respuesta: minimum_binary_tree.py
***

#	Pregunta 4

Crear una función que resuelva el siguiente problema.
Dado un diccionario de n palabras B, identificar si un texto A puede ser separado en palabras B dentro del diccionario. 1 verdadero, 0 falso.
Ejemplo:
```
Input:
n = 12
B = { "i", "like", "sam", "sung", "samsung", "mobile",
"ice","cream", "icecream", "man", "go", "mango" }
A = "ilikesamsung
```

### Respuesta: dict_finder_words.py
***