# Funtion that runs through a spiral-shaped matrix
def spiralPrint(m, n, a):
    k = 0
    l = 0
 
    ''' k - starting row index
        m - ending row index
        l - starting column index
        n - ending column index
        i - iterator '''
 
    while (k < m and l < n):
 
        # Print the first row from
        # the remaining rows
        for i in range(l, n):
            print(a[k][i], end=" ")
 
        k += 1
 
        # Print the last column from
        # the remaining columns
        for i in range(k, m):
            print(a[i][n - 1], end=" ")
 
        n -= 1
 
        # Print the last row from
        # the remaining rows
        if (k < m):
 
            for i in range(n - 1, (l - 1), -1):
                print(a[m - 1][i], end=" ")
 
            m -= 1
 
        # Print the first column from
        # the remaining columns
        if (l < n):
            for i in range(m - 1, k - 1, -1):
                print(a[i][l], end=" ")
 
            l += 1
 
def get_shape(input_matrix):
    import numpy as np

    arr = np.array(input_matrix)
    num_rows, num_cols = arr.shape
    print('shape of array :', num_rows, num_cols)
    return num_rows, num_cols

##############################
# Example Matrix
##############################
a = [[1, 2, 3, 4],
     [5, 6, 7, 8],
     [9, 10, 11, 12],
     [13, 14, 15, 16]
     ]

# First we need get number of columns and rows
num_rows, num_cols = get_shape(a) 
R = num_rows # Number of rows
C = num_cols # Number of Columns
 
# Function Call a spiral printer that that runs through a spiral-shaped matrix while print every element
spiralPrint(R, C, a)